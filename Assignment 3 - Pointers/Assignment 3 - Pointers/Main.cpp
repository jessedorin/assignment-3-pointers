
// Assignment 3 - Pointers
// Jesse Dorin


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function

void SwapIntegers(int* first, int* second) 
{
	int one = *first;
	*first = *second;
	*second = one;
}


// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;
	//int* pI = &first;
	//int* pII = &second;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);
	//SwapIntegers(pI, pII);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}
